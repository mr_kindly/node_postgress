import Ajv from 'ajv';
import UserService from '../services/user.service';
import { Response, Request } from 'express-serve-static-core';
import { schema } from '../ajv/userSchema';

const ajv = new Ajv({ allErrors: true });
const service = new UserService();

class UserController {
    public returnUsers = async (req: Request, res: Response): Promise<Response> => {
        let result = await service.getUsers();
        if (result instanceof Error) {
            return res.status(400).json({ status: 400, message: result.message });
        } else {
            return res.status(200).json({ status: 200, data: result, message: 'Succesfully Users Retrieved' });
        }
    };
    public addUser = async (req: Request, res: Response): Promise<Response> => {
        let result = await service.addUser(req.body);
        if (result instanceof Error) {
            return res.status(400).json({ status: 400, message: result.message });
        } else {
            const test = ajv.compile(schema);
            const isValid = test(result);
            return isValid
                ? res.status(200).json({ status: 200, data: result, message: 'User is succefully added' })
                : res.status(200).json({ status: 200, message: 'not valid' });
        }
    };
    public deleteUser = async (req: Request, res: Response): Promise<Response> => {
        let result = await service.deleteUser(req.params.id);
        if (result instanceof Error) {
            return res.status(400).json({ status: 400, message: result.message });
        } else {
            return res.status(200).json({ status: 200, data: result, message: 'User is succefully deleted' });
        }
    };

    public updateUser = async (req: Request, res: Response): Promise<Response> => {
        let result = await service.updateUser(req.body, req.params.id);
        if (result instanceof Error) {
            return res.status(400).json({ status: 400, message: result.message });
        } else {
            const test = ajv.compile(schema);
            const isValid = test(result);
            return isValid
                ? res.status(200).json({ status: 200, data: result, message: 'User is succefully updated' })
                : res.status(200).json({ status: 200, message: 'not valid' });
        }
    };
}

export default UserController;
