export const schema = {
    type: 'object',
    additionalProperties: true,
    required: ['login', 'password', 'age', 'isdeleted'],
    items: {
        id: {type: 'string'},
        login: { type: 'string' },
        password: { type: 'string' },
        age: { type: 'number' },
        isdeleted: { type: 'boolean' },
    },
};
