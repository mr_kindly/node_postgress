import { Sequelize, Model, DataTypes } from 'sequelize';
const DB_CONNECTION_STRING = 'postgres://test1:test1@db:5432/db-test';

const sequelize: any = new Sequelize(DB_CONNECTION_STRING, { define: {
    timestamps: false
}});

export class User extends Model {
    public id!: string;
    public login!: string;
    public password!: string;
    public age!: number;
    public isdeleted!: boolean;   
}

User.init(
    {
        id: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
        },
        login: {
            type: DataTypes.STRING,
        },
        password: {
            type: DataTypes.STRING,
        },
        age: {
            type: DataTypes.NUMBER,
        },
        isdeleted: {
            type: DataTypes.BOOLEAN,
        },
    },
    {
        sequelize,
        modelName: 'user',
    },
);

export default User;
