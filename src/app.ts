import express from 'express';
import router from './routes/user.route';
import bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());

app.use('/', router);

let port = 7070;

app.listen(port, (): void => {
    console.log('Server is up and running on port number ' + port);
});
