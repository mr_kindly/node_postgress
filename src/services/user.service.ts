import User from '../models/user.model';
import uuid from 'uuid';

class UserService {
    public getUsers = async (): Promise<User[] | Error> => {
        const users = await User.findAll();
        if (users) {
            return users;
        } else {
            return new Error('No users');
        }
    };

    public addUser = async (query: User): Promise<User | Error> => {
        const user = await User.create({
            id: uuid(),
            login: query.login,
            password: query.password,
            age: query.age,
            isdeleted: false,
        });
        if (user) {
            return user;
        } else {
            return new Error('Error while adding user');
        }
    };

    public deleteUser = async (id: string): Promise<String | Error> => {
        const deletedUser = User.update({ isdeleted: true }, { where: { id: id } });
        if (deletedUser) {
            return 'Deleted';
        } else {
            return new Error('User not found');
        }
    };

    public updateUser = async (User: User, id: string): Promise<User | Error> => {
        const updateUser = User.update(User, { where: { id: id } });
        if (updateUser) {
            return updateUser;
        } else {
            return new Error('User not found');
        }
    };
}

export default UserService;
