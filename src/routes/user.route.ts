import express from 'express';
import UserController from '../controllers/user.controller';
const controller = new UserController();
const router = express.Router();

router.get('/users', controller.returnUsers);
router.get('/api', () => console.log("test"));
router.put('/add', controller.addUser);
router.delete('/delete/:id', controller.deleteUser);
router.post('/update/:id', controller.updateUser);

export default router;
