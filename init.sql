CREATE TABLE Users (
  id VARCHAR(100) PRIMARY KEY,
  login VARCHAR(100) not null,
  password VARCHAR(100) not null,
  age INT not null,
  isDeleted BOOLEAN
);

INSERT INTO Users (id, login, password, age, isDeleted)
VALUES ('1', 'User 1', 'ddd', 33, false);

INSERT INTO Users (id, login, password, age, isDeleted)
VALUES ('2', 'User 2', 'ddd', 33, false);

INSERT INTO Users (id, login, password, age, isDeleted)
VALUES ('3', 'User 3', 'ddd', 33, false);
